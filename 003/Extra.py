class Expensive(Exception):
    """ Raised if the purchase is too expensive"""
    pass

try:
    available_money = 500
    purchase = int(input("How much money do you need?"))

    if purchase > available_money:
        raise Expensive
    else:
        print("You can buy it!")
except Expensive:
    print("You don't have enough money to buy this!")