class Calculator:
    def __init__(self, first, second):
        self.first = int(first)
        self.second = int(second)

    def __add__(self):
        return self.first + self.second

    def __sub__(self):
        return self.first - self.second

    def __mul__(self):
        return self.first * self.second

    def __divmod__(self):
        try:
         return self.first/self.second
        except ZeroDivisionError:
            raise ZeroDivisionError("No division with 0")

try:
    x = Calculator(7, p)
    print(x.__mul__())
except NameError:
    raise NameError("Only numbers are accepted")