class Employee:
    def __init__(self, name, surname, department, year):
        self.name = name
        self.surname = surname
        self.department = department
        self.year = year

        if type(self.name) != str or type(self.surname) != str or type(self.department) != str or type(self.year) != int:
            raise TypeError("Wrong type of data")

    def datacheck(self):
        year_1998 = ["Smith", "Williams", "Brown"]
        year_1999 = ["Johnson", "Evans", "Flores"]
        if self.year == 1998 and self.surname in year_1998:
            print('Data has been accepted, {} {} is located in the {} department.'.format(self.name, self.surname, self.department))
        elif self.year == 1999 and self.surname in year_1999:
            print('Data has been accepted, {} {} is located in the {} department'.format(self.name, self.surname, self.department))
        else:
            raise NameError("The data is incorrect")

smith = Employee("John", "Smith", "Finances", 1998)
smith.datacheck()