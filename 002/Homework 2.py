class Graphobject:
    pass



class Rectangle:
    def __init__(self, side_a=4, side_b=5):
        self.side_a = side_a
        self.side_b = side_b

    def printing(self):
        for x in range(self.side_a):
            print("* " * self.side_b)

class Button(Rectangle):
    def __init__(self,button=False):
        super().__init__()
        self.button = button

    def button_pressed(self):
        if self.button == True:
            self.printing()

        else:
            pass

class Mouse_button(Button):
    def __init__(self, click=False):
        super().__init__()
        self.click = click

    def clicking(self):
        if self.click == True:
            self.button = True
            self.button_pressed()
        else:
            pass


mouse = Mouse_button(False)
mouse.clicking()