class Vehicle:
    def __init__(self):
        self.passengers = True
        self.water = True
        self.road = True
        self.cargo = True

    def abilities(self):
        print(type(self).__name__, ":")
        print("The vehicle can transport passengers: ", self.passengers)
        print("The vehicle can drive on water: ", self.water)
        print("The vehicle can drive on roads: ", self.road)
        print("The vehicle can transport cargo: ", self.cargo)


class Car(Vehicle):
    def __init__(self):
        super().__init__()
        self.water = False


class Motorcycle(Vehicle):
    def __init__(self):
        super().__init__()
        self.water = False
        self.cargo = False


class Boat(Vehicle):
    def __init__(self):
        super().__init__()
        self.road = False


class Truck(Car):
    def __init__(self):
        super().__init__()
        self.passengers = False


volvo_FM = Truck()
volvo_FM.abilities()
fjord_ff_coupe = Boat()
fjord_ff_coupe.abilities()
bmv_touring = Car()
bmv_touring.abilities()
yamaha_bt_bulldog = Motorcycle()
yamaha_bt_bulldog.abilities()
