class Vertebrate:
    def live_birth(self):
        return False


class Mammal(Vertebrate):
    def live_birth(self):
        return True


class Bird(Vertebrate):
    pass


class Platypus(Bird, Mammal):
    pass


platypus = Platypus()
print(platypus.live_birth())

for cls in [Vertebrate, Bird, Mammal, Platypus]:
    print(cls.__name__, " : ", cls.mro())
