class Book:

    def __init__(self, name, author, genre, date):
        self.name = name
        self.author = author
        self.genre = genre
        self.date = date

    def __repr__(self):
        return "The book %s by %s is published on %s and has the %s genre" % (self.name, self.author, self.date, self.genre)

    def __str__(self):
        return "The book %s by %s is published on %s and has the %s genre" % (self.name, self.author, self.date, self.genre)

    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return not (self.name == other.name)


book1 = Book("Sherlock", "Conan Doyle", "detective", "1886")
book2 = Book("Orient Express", "Agatha Christie", "detective", "1934")

print(repr(book1))
print(repr(book2))
print(book1)
print(book2)

print('Comparison =', book1 == book2)