class Temp:
    def __init__(self, temperature, param="C"):
        self.temperature = temperature
        self.param = param

    @property
    def convertible(self):
        return ('{} {}'.format(self.temperature, self.param))

    @convertible.setter
    def convertible(self, x):
        self.param = x
        if x == "C":
            print((self.temperature - 32) / 1.8, "°C")
        elif x == "F":
            print('{} {}'.format(self.temperature * 1.8 + 32, self.param))

room = Temp(22)
human = Temp(100, "F")
print(room.convertible)
room.convertible = "F"

print(human.convertible)
human.convertible = "C"


#if self._param == "C":
 #   print(self.temperature * 1.8 + 32, "F")
#elif self._param == "F":
 #   print((self.temperature - 32) / 1.8, "°C")
#return ('{} {}'.format(self.temperature, self._param))