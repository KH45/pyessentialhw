class Book:
    def __init__(self, name, author, genre, date):
        self.name = name
        self.author = author
        self.genre = genre
        self.date = date

    def __repr__(self):
        return "The book %s by %s is published on %s and has the %s genre" % (
            self.name, self.author, self.date, self.genre)


class Bookreview(Book):
    def review(self):
        if self.author == "Conan Doyle":
            print("The book is a timeless classic, 5 stars!")


book1 = Bookreview("Sherlock", "Conan Doyle", "detective", "1886")
print(repr(book1))
book1.review()

book1 = Book("Sherlock", "Conan Doyle", "detective", "1886")
